﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["test"] == "123")
        {
            //string decryptedstr = Convert.FromBase64String(.ToString());            
            WhSsoDecodeToken _decode = new WhSsoDecodeToken(Request.QueryString["wh_sso_login"], "x-whkeyhere", Request.QueryString["wh_s"]);
            Response.Write(_decode.UserId);
            Response.Write("<br>");
            Response.Write(_decode.Firstname + " " + _decode.Lastname);
            Response.Write("<br>");
            Response.Write(_decode.Email);
            Response.Write("<br>");
        }
        else
        {
            WhSsoRedirect _redirect = new WhSsoRedirect("SchoolDomain", "wsapi", "x-whkeyhere");
            string _str = _redirect.GetSsoRedirectUrl("http://localhost/default.aspx?test=123");
            Response.Redirect(_str);
        }

    }
}

public class WhSsoDecodeToken
{
    private string _encryptionKey;
    private string _userId;
    private DateTime _timestamp;
    private string _userName;
    private string _email;
    private string _firstName;
    private string _lastName;
    private string _hostId;
    


    //string loginToken = System.String.Format("{0}||{1}||{2}||{3}||{4}||{5}||{6}", userId, hostId, timestamp, userName, email, userFirst, userLast);

    private static long _unixStart = new DateTime(1970, 1, 1, 0, 0, 0).Ticks;

    /// <summary> 
    /// Use this class to decode the token returned by WhippleHill. 
    /// </summary> 
    /// <param name="encryptionKey">The shared encryption key. Provided by WhippleHill Support.</param> 
    public WhSsoDecodeToken(string token, string encryptionKey, string salt)
    {
        _encryptionKey = encryptionKey;
        decryptToken(token, salt);
    }
    /// <summary> 
    /// These are the properties from the token 
    /// </summary> 
    public string UserId { get { return _userId; } }
    public DateTime TimestampUtc { get { return _timestamp; } }
    public string Username { get { return _userName; } }
    public string Email { get { return _email; } }
    public string Firstname { get { return _firstName; } }
    public string Lastname { get { return _lastName; } }
    public string HostId {
        get { return _hostId; }
    }
    public int TokenAgeInSeconds { get { return (DateTime.UtcNow - TimestampUtc).Seconds; } }
    private void decryptToken(string token, string salt)
    {
        // Decrypt the token 
        string plainToken = DecryptRJ256(token, _encryptionKey, salt);
        // Pull out the fields 
        string[] fields = plainToken.Split(new String[] { "||" }, StringSplitOptions.None);
        _userId = fields[0];
        _hostId = fields[1];
        _userName = fields[3];
        _email = fields[4];
        _firstName = fields[5];
        _lastName = fields[6];
        long timestampTicks = _unixStart + long.Parse(fields[2]) * 10000000;
        _timestamp = new DateTime(timestampTicks, DateTimeKind.Utc);
    }

    private static string DecryptRJ256(string prm_text_to_decrypt, string prm_key, string prm_iv)
    {
        string sEncryptedString = prm_text_to_decrypt;
        RijndaelManaged myRijndael = new RijndaelManaged();
        myRijndael.Padding = PaddingMode.Zeros;
        myRijndael.Mode = CipherMode.CBC;
        myRijndael.KeySize = 256;
        myRijndael.BlockSize = 256;
        byte[] key = null;
        byte[] IV = null;
        key = Encoding.ASCII.GetBytes(prm_key);
        IV = Encoding.ASCII.GetBytes(prm_iv);
        ICryptoTransform decryptor = myRijndael.CreateDecryptor(key, IV);
        byte[] sEncrypted = Convert.FromBase64String(sEncryptedString);
        byte[] fromEncrypt = new byte[sEncrypted.Length + 1];
        MemoryStream msDecrypt = new MemoryStream(sEncrypted);
        CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
        return (Encoding.ASCII.GetString(fromEncrypt)).TrimEnd('\0');
    }
}


public class WhSsoRedirect
{

    string _whSsoUrlRoot;

    string _vendorKey;

    string _encryptionKey;

    /// <summary>

    /// This class knows how to create a properly formed URL that

    /// you can redirect the browser to for a WhippleHill login.

    /// </summary>

    /// <param name="whSsoUrlRoot">The WhippleHill login URL root. Provided by WhippleHill Support.</param>

    /// <param name="vendorKey">The unique vendor id. Provided by WhippleHill Support.</param>

    /// <param name="encryptionKey">The shared encryption key. Provided by WhippleHill Support.</param>

    public WhSsoRedirect(string whSsoUrlRoot, string vendorKey, string encryptionKey)
    {

        _whSsoUrlRoot = whSsoUrlRoot;

        _vendorKey = vendorKey;

        _encryptionKey = encryptionKey;

    }

    /// <summary>

    /// This is the method you call to create the fully formed SSO URL

    /// This URL is the one you redirect to so that WhippleHill will

    /// make sure the user is logged in and return their credentials to you.

    /// </summary>

    /// <param name="returnToUrl"></param>

    /// <param name="salt"></param>

    /// <returns></returns>

    public string GetSsoRedirectUrl(string returnToUrl)
    {

        // Generate a new one-time salt

        string salt = GenerateSalt();

        // encrypt returnToUrl

        string encryptedReturnToUrl = EncryptRJ256(returnToUrl, _encryptionKey, salt);

        // generate WH Login URL

        string redirectUrl = _whSsoUrlRoot + "/app/sso/custom"

        + "?rt_url=" + HttpContext.Current.Server.UrlEncode(encryptedReturnToUrl)

        + "&wh_s=" + salt;

        return redirectUrl;

    }



    public static string EncryptRJ256(string prm_text_to_encrypt, string prm_key, string prm_iv)
    {

        string sToEncrypt = prm_text_to_encrypt;

        RijndaelManaged myRijndael = new RijndaelManaged();

        myRijndael.Padding = PaddingMode.Zeros;

        myRijndael.Mode = CipherMode.CBC;

        myRijndael.KeySize = 256;

        myRijndael.BlockSize = 256;

        byte[] encrypted = null;

        byte[] toEncrypt = null;

        byte[] key = null;

        byte[] IV = null;

        key = Encoding.ASCII.GetBytes(prm_key);

        IV = Encoding.ASCII.GetBytes(prm_iv);

        ICryptoTransform encryptor = myRijndael.CreateEncryptor(key, IV);

        MemoryStream msEncrypt = new MemoryStream();

        CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

        toEncrypt = Encoding.ASCII.GetBytes(sToEncrypt);

        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);

        csEncrypt.FlushFinalBlock();

        encrypted = msEncrypt.ToArray();

        return (Convert.ToBase64String(encrypted));

    }

    private string GenerateSalt()
    {

        // Pick printable characters, since we have to pass the salt in a query param

        string printableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        string newSalt = GenerateKey(32, printableChars, "0123456789");

        return newSalt;

    }

    private static string GenerateKey(int length, string letterPool, string numberPool)
    {



        var chars = letterPool + numberPool;

        var random = new Random();

        var result = new string(

            Enumerable.Repeat(chars, length)

                      .Select(s => s[random.Next(s.Length)])

                      .ToArray());

        return result;



    }

}